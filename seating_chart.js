var banquetStore, banquetTableStore; // must be global for inputs to get hooked up on return
(function(){

seating_chart_get_table_details = function(evt){
   var node = this.getNode();
   var content = node.getAttribute('id');
   var tbl = content.replace(/table_/, '');
   
   //dijit.byId('banquetTableDetail').set('href','/seating_chart/' + Drupal.settings.seating_chart.bnid + '/table/' + tbl);
   if (ds.banquetSeats.active) {
      var tmp = Drupal.seating_chart.banquetSeats.active;
      ds.banquetSeats.active = null;
      seating_chart_banquet_setState(this.id, tmp);

   }
   ds.banquetSeats.active = this;
  // console.log('in',this);
  /* dojox.gfx.fx.animateTransform({
      shape: ds.canvas,
      duration: 500,
      transform: [
         //{name: ''}
         {name: 'translate', start:[0,0], end:[-this.children[0].shape.x, -this.children[0].shape.y]},
         {name: 'scale', start: [1], end: [2]},
         {name: 'original'}
         ]
   }).play();
  */
}


/*
 * callback to handle data for banquet map
 */
seating_chart_load = function(){
   dojo.require('dojo.data.ItemFileWriteStore');
   dojo.require("dijit.form.Form");
   dojo.require("dijit.form.Button");
   dojo.require("dijit.form.FilteringSelect");
   dojo.require("dijit.layout.BorderContainer");
   dojo.require('dojo_toolkit.gfx.tooltip');
            banquetStore = new dojo.data.ItemFileWriteStore({
            url: '/seating_chart/' + Drupal.settings.seating_chart.bnid + '/seats'
         });
         banquetStore._saveCustom = seating_chart_save_registration;
         banquetTableStore = new dojo.data.ItemFileWriteStore({
            url: '/seating_chart/' + Drupal.settings.seating_chart.bnid + '/tables'
         });
         banquetTableStore._saveCustom = seating_chart_save_registration;
   dojo.parser.parse();
   setTimeout(ds.getTableData,100);
}

seating_chart_set_banquet_table_status = function(items, request){
   var tableId = 'table_' + banquetStore.getValue(item, 'table');
   if(items.length > 0){
      seating_chart_banquet_setState(tableId, dojo.byId(tableId), false, 'full');
   }
}

seating_chart_banquet_over = function(evt){
   if (Drupal.seating_chart.banquetSeats.hover){
      // reset old one
      seating_chart_banquet_setState(this.name, Drupal.seating_chart.banquetSeats);
   }
   seating_chart_banquet_setState(this.name,this,true);
   ds.banquetSeats.hover = this;
}

seating_chart_banquet_out = function(evt){
   seating_chart_banquet_setState(this.name, this);
   ds.banquetSeats.hover = null;
}

seating_chart_banquet_setState = function(id, item, over, state){
   //console.log(id,item,over,state);
   var tblId, tbl, color, dsnc = Drupal.settings.seating_chart.colors;

      tblId = id;
      tbl = item;

   
   if (tbl){
      if (over){
         dojo.forEach(tbl.children, function(childN){
            if(childN.shape.type == 'circle'){
               childN.setFill(dsnc.tblHover);
               childN.setStroke({
                   color: '#fff',
                   width: '1.5'
               }); // do not set state, allow it to revert
            } else if (childN.shape.type == 'text'){
      //         childN.setFill('#fff');
            }
         });
         return;
      }
      
      if (tbl == ds.banquetSeats.active) {
         state = 'active';
      }
      switch (state){
         case 'full':
            color = dsnc.tblFull;
            break;
         case 'active':
            color = dsnc.tblActive;
            break;
         default:
            color = dsnc.tblNormal;
      }
      dojo.forEach(tbl.children, function(childN){
         if(childN.shape.type == 'circle'){
            childN.setFill(color);
            childN.setStroke({
               color: '#000',
               width: '1.5'
            });
         } else if (childN.shape.type == 'text') {
       //     childN.setFill('#000');
         }
      });
      
   }
}

seatingChartReserve = function(evt) {
   var name, m = this.name.match(/tbl(\d+)s(\d+)$/);
   this.set('disabled','disabled');
   switch (this.value) {
      case 'v':
         name = 'VIP';
         break;
      case 'u':
         name = 'Unavailable';
         break
      default:
         name = dss.user;
   }
   var sv = {
      bnid: dss.bnid,
      uid: dss.uid,
      table: m[1],
      seat: m[2],
      name: name,
      reserved: true,
      value: this.value,
      sid: 1,
      nid: 0,
      action: 'add'
   }
   if (!sv.sid) {
      sv.uid = -1;
      sv.sid = 0;
   }
   ds.toSave.push(sv);
   ds.enableConfirm();
         dojo.query('[name='+this.name+']').forEach(function(item){dijit.getEnclosingWidget(item).set('disabled','disabled')});
   //banquetStore.setValue(item, 'table', m[1]);
   //banquetStore.setValue(item, 'seat', m[2]);
   //if (this.value > 0){
     // banquetStore.setValue(item, 'needs_seat', false);
   //}
   
}

seatingChartChangeType = function(evt) {
   var name, m = this.name.match(/tbl(\d+)s(\d+)$/);
   //console.log(evt,this);
   var sv = {
      bnid: dss.bnid,
      uid: null,
      table: m[1],
      seat: m[2],
      name: null,
      reserved: false,
      value: this.value, // converted to type on other end
      sid: null,
      nid: null,
      action: 'add'
   }
   ds.toSave.push(sv);
   ds.enableConfirm();
   dojo.query('[name='+this.name+']').forEach(function(item){dijit.getEnclosingWidget(item).set('disabled','disabled')});
}

seating_chart_save_registration = function(completeCallback, errCallback){
   var aroundNode = ds.activeTT;
   if (ds.activeTT) ds.activeTT.hideTooltip();
   dojo.xhrPost({
      postData: dojo.toJson(Drupal.seating_chart.toSave),
      url: '/seating_chart/' + Drupal.settings.seating_chart.bnid + '/post',
      load: function(ret){
         completeCallback();
         Drupal.seating_chart.toSave = [];
         banquetStore.close();
         var msg = 'Changes saved.';
         //dijit.byId('banquetTableDetail').set('content', 'Changes saved.');
         for (var i=0; i< ret.length; i++){
            //console.log(i, ret);
            if (i['return']){
               //dijit.byId('banquetTableDetail').set('content', i['return']);
               msg = i['return'];
            }
         }
         if (aroundNode) {
            aroundNode.showTooltip(msg);
            ds.activeTT = aroundNode;
            ds.TTTimout = setTimeout(ds.hideTooltip, 5000);
         } else {
            dijit.byId('banquetTableDetail').set('content',msg);
         }
         ds.getTableData();
      },
      error: errCallback
   });
}

banquetSeatChange = function(value){
   var m = this.id.match(/tbl(\d+)s(\d+)$/);
   var item = this.item;
   var sv = {
      bnid: Drupal.settings.seating_chart.bnid,
      uid: banquetStore.getValue(item,'uid'),
      table: m[1],
      seat: m[2],
      reserved: true,
      name: banquetStore.getValue(item,'name'),
      sid: banquetStore.getValue(item,'sid'),
      nid: value,
      action: 'add'
   }
   if (!sv.sid) {
      sv.uid = -1;
      sv.sid = 0;
   }
   Drupal.seating_chart.toSave.push(sv);
   banquetStore.setValue(item, 'table', m[1]);
   banquetStore.setValue(item, 'seat', m[2]);
   if (this.value > 0){
      banquetStore.setValue(item, 'needs_seat', false);
   }
   
}

banquetSetMax = function(evt){
   var m = this.name.match(/tbl(\d+)Max$/);
   var max = this.value;
   var sv = {
      'table': m[1],
      'max': this.value,
      'action': 'max'
   }
   Drupal.seating_chart.toSave.push(sv);
   ds.enableConfirm();
}
banquetSetShape = function(evt){
   var m = this.name.match(/tbl(\d+)Shape$/);
   var shape = this.value;
   var sv = {
      'table': m[1],
      'shape': this.value,
      'action': 'shape'
   }
   Drupal.seating_chart.toSave.push(sv);
   ds.enableConfirm();
}

banquetCancelSeat = function(evt){
   var m = this.id.match(/tbl(\d+)s(\d+)$/);
   var sv = {
      bnid: Drupal.settings.seating_chart.bnid,
      table: m[1],
      seat: m[2],
      reserve: false,
      action: 'cancel'
   };
   if (!sv.sid) {
      sv.uid = -1;
      sv.sid = 0;
   }
   dojo.style(this.domNode.parentNode,{'text-decoration':'line-through','color':'#ff0000'});
   Drupal.seating_chart.toSave.push(sv);
   ds.enableConfirm();
}

banquetSave = function(evt){
   banquetStore.save();
}

   Drupal.seating_chart = {
      banquetSeats: {},
      toSave: [],
      surface: null, // reference to dojox.gfx surface
      canvas: null, // reference to parent group, for zooming
      
      activeTT: null, // reference to a gfx object showing a tooltip
      
      hideTooltip: function() {
         if (ds.activeTT) {
            ds.activeTT.hideTooltip();
            var cp = dijit.byId(ds.activeCP);
            console.log('hide tt',cp);
            if (cp) cp.destroyRecursive();
            ds.activeTT = null;
            ds.activeCP = cp = null;
         }
      },
      enableConfirm: function() {
         dojo.query('.reserveConfirmButton').forEach(function(item){dijit.getEnclosingWidget(item).set('disabled','')});
      },
      
      getTableData: function(){
         dojo.xhrGet({
            url: '/seating_chart/'+Drupal.settings.seating_chart.bnid +'/tables',
            handleAs: 'json',
            load: function(tblData){
               dss.tableData = tblData;
      
               /**
                * draw tables
                */
               var tableColumns = dss.tableColumns;
               
               //find the largest column
               var column, i,tblNum = 1, m, shape, columnHeight, largeColumnHeight = 0;
               for(column in tableColumns){
                  columnHeight = 0;
                  for (i=0; i< tableColumns[column].value;i++){
                     if (tblData && tblData.tables[tblNum]) {
                        shape = tblData.tables[tblNum].shape;
                        m = tblData.tables[tblNum].max;
                     } else {
                        shape = dss.shape;
                        m = dss.max;
                     }
                     switch (shape) {
                        case 'round':
                           columnHeight += dss.layout.round.height + dss.layout.spacing.height;
                           break;
                        case 'rect':
                           columnHeight += Math.ceil(m/2) * dss.layout.rect.height + dss.layout.spacing.height;
                           break;
                     }
                     tblNum++;
                  }
                  columnHeight -= dss.layout.spacing.height;
                  dss.tableColumns[column].height = columnHeight;
                  if(columnHeight > largeColumnHeight){
                     largeColumnHeight = columnHeight;
                  }
                  dss.largeColumnHeight = largeColumnHeight;
               }
               
               if (ds.surface) {
                  ds.surface.clear();
                  ds.drawMap();
               } else {
                  //create the drawing surface
                  //dojo.style(dojo.byId('banquetGfx'), 'background', 'url("/' + Drupal.settings.seating_chart.bmap + '") no-repeat 0 0');
                  ds.surface = dojox.gfx.createSurface('banquetGfx', dss.layout.surface.width, dss.layout.surface.height);
                  //draw tables
                  ds.surface.whenLoaded(ds.drawMap);
               }
            }
         });
      },
      zoomIn: function(evt) {
         //console.log(evt);
         var posx = 0, posy=0;
         if (!evt) evt = window.event;
         if (evt.pageX || evt.pageY) {
            posx = evt.pageX;
            posy = evt.pageY;
         } else if (evt.clientX || evt.clientY) {
            posx = evt.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
            posy = evt.clientY + document.body.scrollTop + document.documentElement.scrollTop;
         }
         //console.log(evt.gfxTarget.getUID(), evt.gfxTarget.rawNode.getBBox());
         var t = {x: evt.layerX, /*posx*/ y: evt.layerY /*posy*/}
         ds.currentZoom = [2.5,t.x, t.y];
         dojox.gfx.fx.animateTransform({
            shape: ds.canvas,
            duration: 500,
            transform: [
               {name: 'scaleAt',start:[1,t.x,t.y], end:ds.currentZoom},
//               {name: 'original'}
            ],
            onEnd: function() {
               ds.isZoomed = true;
               //ds.zoomOutTimout = setTimeout(ds.zoomOut, 5000);
            }
         }).play();         
      },
      zoomOut: function() {
         var def = dojox.gfx.fx.animateTransform({
            shape: ds.canvas,
            duration: 500,
            transform: [
               {name: 'scaleAt',start:ds.currentZoom, end:[1,0,0]}
        //       {name: 'original'}
            ],
            onEnd: function(){
               ds.isZoomed = false;
               ds.currentZoom = [0,0,0];
               clearTimeout(ds.zoomOutTimout);
            }
         }).play();
         
      },
      seatX: 1,
      seatX2: 2,
      seatY: 'A',
      
      drawMap: function(){
            //surface.openBatch();
            ds.canvas = ds.surface.createGroup();
            if (dss.bgimage) ds.canvas.createImage({src: '/'+dss.bgimage,height:dss.layout.surface.height, width: dss.layout.surface.width});
            var c = ds.canvas.createRect({x:-500,y:-500,width:dss.layout.canvas.width,height:dss.layout.canvas.height}).setFill(dss.colors.background);
            c.connect('click', ds.canvasClick);
            new dojox.gfx.Moveable(ds.canvas);
            var group, align, seati, theTable, tblData = dss.tableData, tableColumns = dss.tableColumns;
            var count = 1, largeColumnHeight = dss.largeColumnHeight;
            var ii = 0, offX, offY, width, height;
            
            seatX = 1;
            seatX2 = 2;
            seatY = 'A';
            
            //display settings for table drawing
            var seatCount, seatFill, seatDefault = 8;
            var dsnb = Drupal.seating_chart.banquetSeats;
            var dsnc = Drupal.settings.seating_chart.colors;
            
            //adjust canvas margins to align on map background
            var margY = dss.layout.position.top;
            if (dss.layout.position.y == 'center') {
               margY += (dss.layout.surface.height - largeColumnHeight ) / 2;
            }
            var margX = (dss.layout.position.x == 'center') ? (dss.layout.surface.width - (tableColumns.length*dss.layout.spacing.width))/2 + dss.layout.position.left : dss.layout.position.left;
            
            for(column in tableColumns){
               offX = dss.layout.spacing.width * column + margX;
               offY = margY;
               if (dss.layout.position.y == 'center') {
                  offY += (largeColumnHeight - tableColumns[column].height)/2;
               }
               for(ii = 0; ii < tableColumns[column].value; ii++){
                  theTable = tblData.tables[count];
                  if (!theTable) {
                     theTable = {
                        shape: dss.shape,
                        max: dss.max
                     }
                     dss.tableData.tables[count] = theTable;
                  }
                  switch (theTable.shape) {
                     case 'round':
                        height = dss.layout.round.height;
                        width = dss.layout.round.width;
                        break;
                     case 'rect':
                        height = Math.ceil(theTable.max/2) * dss.layout.rect.height;
                        width = dss.layout.rect.width * 2;
                        break;
                  }
                  
                  dss.tableData.tables[count].seatX = seatX;
                  dss.tableData.tables[count].seatY = seatY;
                  dsnb[count] = ds.drawTable(theTable, offX, offY, width, height, count);
                  count++;
                  offY += height + dss.layout.spacing.height;
               }
               seatX += 2;
               seatX2 += 2;
               seatY = 'A';
            }
            //surface.closeBatch();
         },
      /*
       tableDef: object with following properties:
       - shape - rect or round
       - max - number of seats to render
       - [1..n] data - sparse array of seats taken
       - width (optional) - width - default 15
       - height (optional) - height - default 15
       @param offX {integer} X offset
       @param offY {integer} Y offset
       @param width {integer} Width
       @param height {integer} Height
       
       @returns {object} dojox.gfx group that can be added
      */
      drawTable: function(tableDef, offX, offY, width, height, count) {
         var shape,group = ds.canvas.createGroup();
         var label;
         if (tableDef) {
            shape = tableDef.shape;
         } else {
            shape = dss.shape;
         }
         var seati, seatFill, seatCount = (tableDef && tableDef.max) ? tableDef.max : dss.max;
         switch (shape) {
            case 'round':
                  //actual drawing
                  for(seati = 1; seati <= seatCount; seati++){
                     seatFill = (tableDef && tableDef[seati]) ? dsnc[tableDef[seati].type+tableDef[seati].reserved] : dsnc.r0;
                     group.createRect({x: offX, y: offY, height: dss.layout.round.seatwidth,  width: dss.layout.round.seatwidth, seat: seati }).setStroke('#7c7c7c').setFill(seatFill).setTransform([dojox.gfx.matrix.rotategAt(seati * (360 / seatCount), offX + dss.layout.round.radius, offY + dss.layout.round.radius)]);
                  }
                  group.createCircle({cx: offX + dss.layout.round.radius, cy: offY + dss.layout.round.radius, r: dss.layout.round.radius, table: count }).setFill('white').setStroke({width: '1.5', color: 'black'});
                  group.createText({x: offX + dss.layout.round.radius, y: offY + dss.layout.round.radius + 4, text: count, align: 'middle'}).setFill('black');
               break;
            case 'rect':
               // draw table first
               // seat height/width: 15,+1
               // row width: 31
               // height: 15*(max(seatCount/2))
               var currY = offY;
               group.createRect({x: offX, y: offY, height: (dss.layout.rect.height * Math.ceil(seatCount/2)), width: dss.layout.rect.width, table: count}).setFill('white').setStroke({width:'1.5', color: 'black'});
               for (seati = 1; seati <= seatCount; seati++){
                  label = seatY + seatX;
                     seatFill = (tableDef && tableDef[seati]) ? dsnc[tableDef[seati].type+tableDef[seati].reserved] : dsnc.r0;
                  group.createRect({x: offX, y:currY, height: dss.layout.rect.height, width: dss.layout.rect.seatwidth, seat: seati}).setStroke('#7c7c7c').setFill(seatFill);
                  group.createText({x: offX + dss.layout.rect.seatwidth/2, y:currY + dss.layout.rect.height/2 +2, text: label, align: 'middle'}).setFont({'size':'4pt'}).setFill('black');
                  seati++;
                  label = seatY + seatX2;
                  seatFill = (tableDef && tableDef[seati]) ? dsnc.r1: dsnc.r0;
                     seatFill = (tableDef && tableDef[seati]) ? dsnc[tableDef[seati].type+tableDef[seati].reserved] : dsnc.r0;
                  group.createRect({x: offX + dss.layout.rect.seatwidth, y:currY, height:dss.layout.rect.seatheight, width: dss.layout.rect.seatwidth, seat:seati}).setStroke('#7c7c7c').setFill(seatFill);
                  group.createText({x: offX + dss.layout.rect.seatwidth *1.5, y:currY + dss.layout.rect.height/2 + 2, text: label, align: 'middle'}).setFont({'size':'4pt'}).setFill('black');
                  
                  currY = currY + dss.layout.rect.seatheight;
                  seatY = ds.nextLetter(seatY);
               }
               
               break;
         }
         //group.createCircle({cx: offX, cy: offY, r:3}).setFill('orange');
         group.connect('onmouseover',dojo.hitch(group, seating_chart_banquet_over));
         group.connect('onmouseout',dojo.hitch(group, seating_chart_banquet_out));
         group.connect('onmousedown', dojo.hitch(group, ds.tableClick ));
   
         dojo.style(group.rawNode, 'cursor', 'pointer');
         dojo.attr(group.rawNode, 'id', 'table_'+count);
         return group;
      },
      canvasClick: function(evt) {
         //console.log('canvasClick', evt,this);
         if (ds.isZoomed) {
            ds.zoomOut(evt);
         }
         if (ds.activeTT) {
            ds.activeTT.hideTooltip();
            dijit.byId(ds.activeCP).destroyRecursive();
            ds.activeTT = null;
            ds.activeCP = null;
         }
      },
      tableClick: function(evt) {
         if (!ds.isZoomed) {
            ds.zoomIn(evt);
           // ds.zoomOutTimout = setTimeout(ds.zoomOut, 5000);
         }
         ds.getTableDetails(this, evt);
         //dojo.hitch(this,seating_chart_get_table_details)();
      },
      
      getTableDetails: function(n, evt) {
         var node = n.getNode();
         var content = node.getAttribute('id');
         var tbl = content.replace(/table_/, '');
         var x = dss.tableData.tables[tbl].seatX;
         var y = dss.tableData.tables[tbl].seatY;
         if (dss.open_tt == 1) {
            dojo.xhrGet({
               url: '/'+dss.tabledetail+'/' + dss.bnid + '/table/' + tbl +'/'+x+'/'+y,
               load: function(data){
                  var show = '<div data-dojo-type="dijit.layout.ContentPane" id="xxxTooltipCP">'+data+'</div>';
                  n.showTooltip(show, ['after','above-centered']);
                  dojo.parser.parse();
                  ds.activeTT = n;
                  ds.activeCP = 'xxxTooltipCP';
               }
            });
         } else {
            dijit.byId('banquetTableDetail').set('href','/'+dss.tabledetail+'/' + Drupal.settings.seating_chart.bnid + '/table/' + tbl);
         }
         
         if (ds.banquetSeats.active) {
           var tmp = Drupal.seating_chart.banquetSeats.active;
           ds.banquetSeats.active = null;
           seating_chart_banquet_setState(n.id, tmp);
     
         }
         ds.banquetSeats.active = n;
       
      },
      
      isZoomed: false,
      currentZoom: [1, 0, 0],
      zoomOutTimeout: null,  // setTimeout handle
      
      nextLetter: function(letter) {
         var lastLetter = letter.charCodeAt(letter.length - 1);
         var nextLetter = lastLetter+1;
         var start = letter.substr(0, letter.length-1);
         if (lastLetter == 122 || lastLetter == 90) {
            nextLetter = lastLetter - 25;
            if (start.length > 0) {
               start = ds.nextLetter(start);
            } else {
               // add an a...
               start = String.fromCharCode(nextLetter);
            }
         }
         return start + String.fromCharCode(nextLetter);
      }
   }
   var ds = Drupal.seating_chart;
   var dss = Drupal.settings.seating_chart;
   var dsnb = ds.banquetSeats;
   var dsnc = dss.colors;
   
})();
