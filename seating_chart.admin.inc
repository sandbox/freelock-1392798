<?php

function seating_chart_admin_settings() {
  $form['seating_chart_colors'] = array(
    '#type' => 'fieldset',
    '#title' => t('Color scheme'),
    '#weight' => -1,
  );
  
  $form['seating_chart_colors']['seating_chart_r0'] = array(
    '#type' => 'textfield',
    '#title' => t('Regular, available'),
    '#default_value' => variable_get('seating_chart_r0', '#fff'),
    '#size' => 8,
  );
  $form['seating_chart_colors']['seating_chart_r1'] = array(
    '#type' => 'textfield',
    '#title' => t('Regular, reserved'),
    '#default_value' => variable_get('seating_chart_r1', '#00f'),
    '#size' => 8,
  );
  $form['seating_chart_colors']['seating_chart_a0'] = array(
    '#type' => 'textfield',
    '#title' => t('Type A, available'),
    '#default_value' => variable_get('seating_chart_a0', '#fff'),
    '#size' => 8,
  );
  $form['seating_chart_colors']['seating_chart_a1'] = array(
    '#type' => 'textfield',
    '#title' => t('Type A, reserved'),
    '#default_value' => variable_get('seating_chart_a1', '#00f'),
    '#size' => 8,
  );
    $form['seating_chart_colors']['seating_chart_b0'] = array(
    '#type' => 'textfield',
    '#title' => t('Type B, available'),
    '#default_value' => variable_get('seating_chart_b0', '#fff'),
    '#size' => 8,
  );
  $form['seating_chart_colors']['seating_chart_b1'] = array(
    '#type' => 'textfield',
    '#title' => t('Type B, reserved'),
    '#default_value' => variable_get('seating_chart_b1', '#00f'),
    '#size' => 8,
  );
  $form['seating_chart_colors']['seating_chart_v1'] = array(
    '#type' => 'textfield',
    '#title' => t('VIP, reserved'),
    '#default_value' => variable_get('seating_chart_v1', '#fff'),
    '#size' => 8,
  );
  $form['seating_chart_colors']['seating_chart_u1'] = array(
    '#type' => 'textfield',
    '#title' => t('Unavailable, reserved'),
    '#default_value' => variable_get('seating_chart_u1', '#00f'),
    '#size' => 8,
  );
  $form['seating_chart_colors']['seating_chart_tblhover'] = array(
    '#type' => 'textfield',
    '#title' => t('Table Hover Color'),
    '#default_value' => variable_get('seating_chart_tblhover', '#4f91cd'),
    '#size' => 8,
  );
  $form['seating_chart_colors']['seating_chart_seathover'] = array(
    '#type' => 'textfield',
    '#title' => t('Seat Hover Color'),
    '#default_value' => variable_get('seating_chart_seathover', '#4f91cd'),
    '#size' => 8,
  );
  $form['seating_chart_colors']['seating_chart_tblfull'] = array(
    '#type' => 'textfield',
    '#title' => t('Table Full Color'),
    '#default_value' => variable_get('seating_chart_tblfull', '#98012e'),
    '#size' => 8,
  );
  $form['seating_chart_colors']['seating_chart_tblactive'] = array(
    '#type' => 'textfield',
    '#title' => t('Table Active Color'),
    '#default_value' => variable_get('seating_chart_tblactive', '#ff0000'),
    '#size' => 8,
  );
  $form['seating_chart_colors']['seating_chart_seatactive'] = array(
    '#type' => 'textfield',
    '#title' => t('Seat Active Color'),
    '#default_value' => variable_get('seating_chart_seatactive', '#ff0000'),
    '#size' => 8,
  );
  $form['seating_chart_colors']['seating_chart_tblnormal'] = array(
    '#type' => 'textfield',
    '#title' => t('Table Normal Color'),
    '#default_value' => variable_get('seating_chart_tblnormal', '#ff0000'),
    '#size' => 8,
  );
  $form['seating_chart_colors']['seating_chart_background'] = array(
    '#type' => 'textfield',
    '#title' => t('Background Color'),
    '#default_value' => variable_get('seating_chart_background', '#ffffff'),
    '#size' => 8,
  );
  $form['seating_chart_default_shape'] = array(
    '#type' => 'radios',
    '#title' => t('Default table shape'),
    '#default_value' => variable_get('seating_chart_default_shape', 'round'),
    '#options' => array('round'=>'Round','rect'=>'Rectangle'),
    '#description' => t('The content type for the %add-child link must be one of those selected as an allowed book outline type.', array('%add-child' => t('Add child page'))),
    '#required' => TRUE,
  );
  $form['seating_chart_default_max'] = array(
    '#type' => 'textfield',
    '#title' => t('Default # of seats per table'),
    '#default_value' => variable_get('seating_chart_default_max', 8),
    '#size' => 4,
  );
  $form['seating_chart_view'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of view that loads seating chart data'),
    '#default_value' => variable_get('seating_chart_view', 'seating_chart'),
    '#size' => 20,
  );
  $form['seating_chart_bgimage_field'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of cck field that contains optional background graphic'),
    '#default_value' => variable_get('seating_chart_bgimage_field', 'field_seating_chart'),
    '#size' => 20,
  );
  $form['seating_chart_tabledetail_menucallback'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of menu callback to load table detail pane. Must take nid in arg 1 and table id in arg 3.'),
    '#default_value' => variable_get('seating_chart_tabledetail_menucallback','seating_chart'),
    '#size' => 20,
  );
  $form['seating_chart_layout'] = array(
    '#type' => 'fieldset',
    '#title' => 'Seating Chart Layout',
  );
  $form['seating_chart_layout']['seating_chart_layout'] = array(
    '#type' => 'textarea',
    '#default_value' => variable_get('seating_chart_layout','{
  "round":  {
   "height": 30,
   "weight": 30,
   "radius": 15,
   "seatwidth": 7.5
  },
  "rect": {
   "height": 10,
   "width": 20,
   "seatheight": 10,
   "seatwidth": 10
  },
  "position": {
   "x": "left",
   "y": "top",
   "top": 10,
   "right": 0,
   "bottom": 10,
   "left": 50
  },
  "spacing": {
   "width": 50,
   "height": 10
  },
  "canvas": {
   "width":  1543,
   "height": 1577
  },
  "surface": {
   "width": 543,
   "height": 577
  },
  "layout": "columns" 
}'),
    '#title' => t('Seating Chart Layout'),
    '#description' => t('JSON-formatted object with seating chart details'),
  );
  return system_settings_form($form);
}
